from app.model.cell_automata import CellAutomata 
from app.model.state import State
from app.model.state import SimulationResult
import app.model.common as common
import app.core.validator.state_validator as validator

def gen_ress() -> CellAutomata:
    p1 = common.Person(col = 1, row = 1)
    o1 = common.Obstacle(col = 2, row = 1)
    t = common.Target(col = 10, row = 10)

    s = State(id = 1, persons = [p1], obstacles = [o1], target = t)
    assert(validator.is_valid(s))

    ca = CellAutomata(id = 1, row_size = 25, col_size = 25, simsteps = 5, state = s)

    p2 = common.Person(col = 1, row = 1)
    p3 = common.Person(col = 2, row = 1)
    p4 = common.Person(col = 3, row = 1)

    si = SimulationResult(step=[[p2],[p3],[p4]])

    return [ca,si]