from fastapi import APIRouter, Depends, HTTPException

from app.core.gen_simulation import gen

from app.model.cell_automata import CellAutomata
from app.model.state import SimulationResult

from app.core.validator import state_validator

router = APIRouter()

#api for the backend.
@router.post("/simulate" , response_model=SimulationResult)
def simulate(config: CellAutomata):
    if(not state_validator.is_valid(config.state)):
        raise HTTPException(status_code=400, detail="Input not Valid")
    output = gen(config)
    # print(output)
    return output