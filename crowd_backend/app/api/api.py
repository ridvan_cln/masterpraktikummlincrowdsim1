from fastapi import APIRouter
from .router import simulation

api_router = APIRouter()

api_router.include_router(
    simulation.router,
    tags=["simulate"],
)