import typing as T

from pydantic import BaseModel

from .common import Person, Obstacle, Target

class State(BaseModel):
    persons: T.List[Person]
    obstacles: T.List[Obstacle]
    target: Target

class SimulationResult(BaseModel):
    step: T.List[T.List[Person]]