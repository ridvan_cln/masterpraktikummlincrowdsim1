from pydantic import BaseModel
import typing as T
import uuid

class Position(BaseModel):
    row: int
    col: int

class Person(Position):
    rep = 'P'
    speed: int = 1

class PersonID(Person):
    uid = uuid.uuid4()

class Obstacle(Position):
    rep = 'O'

class Target(Position):
    rep = 'T'