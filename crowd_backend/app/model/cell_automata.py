import typing as T

from pydantic import BaseModel

from .state import State

class CellAutomata(BaseModel):
    id: str
    row_size: int
    col_size: int
    simsteps: int
    state: State