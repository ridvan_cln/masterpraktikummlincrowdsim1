import typing as T

from app.model.state import State

# Validator for the inputs.
def is_valid(state: State):
    list_of_pos = []
    for person in state.persons:
        list_of_pos.append((person.row, person.col))
    for obstacle in state.obstacles:
        list_of_pos.append((obstacle.row, obstacle.col))
    list_of_pos.append((state.target.row, state.target.col))
    return len(list_of_pos) == len(set(list_of_pos))