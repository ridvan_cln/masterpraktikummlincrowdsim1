from app.ressources.data import gen_ress

from app.model.cell_automata import CellAutomata
from app.core.simulation.simulation import pipeline

# entrypoint for backend.
def gen(ca: CellAutomata):
    print(ca)
    result = pipeline(ca = ca, steps = ca.simsteps)
    # _, si = gen_ress() 
    return result