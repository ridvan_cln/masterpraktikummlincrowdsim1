import typing as T

from app.model.cell_automata import CellAutomata
from app.model.state import SimulationResult
from app.model.common import Person, Obstacle, Target

from scipy.sparse.csgraph import dijkstra
from scipy.spatial import distance

import math as m
import numpy as np

class Evolution:

    """ Evolution class for the evolution operator to store 
    attributes which are need all the time, like the distance map.	"""

    def __init__(self, rows, cols, os, t):
        self.rows = rows
        self.cols = cols
        self.os = os
        self.t = t
        self.r_max = self.r_max_fct()
        self.distanceUtility = self.calcDistanceMap()

    """ Used in the formlua in the person avoidance
        :rtype: r_max for porson avoidance
    """    

    def r_max_fct(self):
        return 1.4 #this should be changed

    """ Creates the utility function for the evolution operator.
        :rtype: The Pathcost of a cell to the target.
    """    
    def calcDistanceMap(self):
        target = self.t
        dijkstra_matrix = self.get_dijkstra_distance_matrix()
        dist_map = {}
        for i in range(1, self.rows+1):
            for j in range(1, self.cols+1):
                dist_to_target = dijkstra_matrix[(i-1)*self.cols + (j-1)][(target.row-1)*self.cols+ (target.col-1)]
                dist_map.update({(i, j): dist_to_target})
        return dist_map

    """ Find all neighbouring cells.

        :type row:int:
        :param row:int: cell
    
        :type col:int:
        :param col:int: cell

        :rtype: All neighbours of cell.
    """    
    def getNeighbors(self, row: int, col: int):
        Adjacents = {(-1, 1), (0, 1), (1, 1), (-1, 0), (0, 0), (1, 0), (-1, -1), (0,-1), (1,-1)}
        adj = []
        for i,j in Adjacents:
            check = [row+i, col+j]
            if 0 < check[0] <= self.rows and 0 < check[1] <= self.cols:
                adj.append(check)
        return adj

    """ Euclidean distance, like documented in scipy.

        :type cell1_row:int: 
        :param cell1_row:int: cell 1 
    
        :type cell1_col:int:
        :param cell1_col:int: cell 1
    
        :type cell2_row:int:
        :param cell2_row:int: cell 2
    
        :type cell2_col:int:
        :param cell2_col:int: cell 2
    
        :rtype: euclidean distance
    """    
    def calcDistance(self, cell1_row: int, cell1_col: int, cell2_row: int, cell2_col: int):
        dist = distance.euclidean((cell1_row,cell1_col), (cell2_row,cell2_col))
        return dist

    """ Cost for an obstacle. Not needed anymore.

        :type cell:
        :param cell: For a cell.
    
        :rtype: high or no cost.
    """ 
    def addObstacleAvoidance(self, cell):
        temp = Obstacle(row=cell[0],col=cell[1])
        if temp in self.os:
            return 999
        return 0

    """ Adding the cost to a cell, so a Persons starts to avoid other Persons
        :type cell:
        :param cell: Current neighbouring cell
    
        :type L_pers:
        :param L_pers: All persons except the current Person in the evolution step.
    
        :rtype:
    """    
    def addPersonAvoidance(self, cell, L_pers):
        total_dist = 0.0
        for p in L_pers:
            #if p != pers:
            temp = self.calcDistance(cell[0], cell[1], p.row, p.col)
            if temp < self.r_max:
                c = np.exp(1/(temp**2 - self.r_max**2))
            else:
                c = 0.0
            total_dist += c
        return total_dist

    """ Is a Person in the current neighbouring cell? If yes, then do not move to this cell.
        :type x:
        :param x: potential move of a Person.
    
        :type y_array:
        :param y_array: All other Persons
    
        :rtype: Boolean
    """   
    def compare(self, x, y_array) -> bool:
        for i in y_array:
            if x.row == i.row and x.col == i.col:
                return True
        return False

    """ Finding the lowest cost of all neighbours.

        :type neigh:T.List:
        :param neigh:T.List: All neighbouring cells.
    
        :type p:Person:
        :param p:Person: The current Person.
    
        :type resx:
        :param resx: All others Persons except the current.

        :rtype: Cell wit lowest cost.
    """    
    def best_neighbour(self, neigh: T.List, p: Person, resx) -> T.Tuple:
        if [self.t.row, self.t.col] in neigh:
            mindist = (p.row,p.col)
        else:
            neigh = [i for i in neigh if not self.compare(Person(row=i[0],col=i[1]), resx)]
            mindist = neigh[np.argmin([self.distanceUtility.get((i[0],i[1])) + self.addPersonAvoidance(i, resx) for i in neigh])]

        if Obstacle(row = mindist[0], col= mindist[1]) in self.os:
            mindist = [p.row, p.col]

        return mindist

    """ The evolution of all Person. Of every Person get all neighbours. Move the 
        Person to the cell with the smallest cost to the target. Person in a cage,
        will not cross obstacles. The extra Loop is the speed of one Person. Example:
        A Person with speed 2 will do two steps and others will stand still. (iterative)
        cost = distance + person avoidance
        :type x: List 
        :param x: All Persons of the Simulation.
    
        :rtype: The movement of all Persons
    """    
    def evolute(self, x) -> T.List[T.List[Person]]:
        res = []
        last_res = []
        while x:
            neigh = self.getNeighbors(x[0].row,x[0].col)
            current_p = x.pop(0)
            resx = last_res + x
            mindist = self.best_neighbour(neigh, current_p, resx)
            
            for _ in range(1,current_p.speed):
                p = Person(row=mindist[0],col=mindist[1],speed=current_p.speed)
                neigh = self.getNeighbors(p.row,p.col)
                mindist = self.best_neighbour(neigh, p, resx)
                res.append([p] + resx)
            
            last_res.append(Person(row=mindist[0],col=mindist[1],speed=current_p.speed))
        return res+[last_res]

    """ Creates a matrix, exclude obstacles. Distance between two adj. nodes is the euclidean distance.
        :rtype: Return of a distance graph. Shortest path from all nodes to all nodes.
    """    
    def get_distances_graph(self):
        distances_graph = []
        for i in range(1, self.rows+1):
            for j in range(1, self.cols+1):
                distances_for_one_cell = [0]*self.rows*self.cols
                if Obstacle(row=i, col=j) not in self.os:
                    neighbours = self.getNeighbors(i, j)
                    for k, l in neighbours:
                        if (i, j) != (k, l) and Obstacle(row=k, col=l) not in self.os:
                            distance = self.calcDistance(i, j, k, l)
                            distances_for_one_cell[(k-1)*self.cols + (l-1)] = distance
                distances_graph.append(distances_for_one_cell)
        return distances_graph

    """ Dijkstra using scipy
        :rtype: Distance Matrix as documented in scipy
    """
    def get_dijkstra_distance_matrix(self):
        dist_matrix = dijkstra(csgraph=self.get_distances_graph(), directed=False)
        return dist_matrix

""" This function provides the entrypoint of our Simulation. It provides an update scheme
    as stated in the exercies. The Return value is used by the frontend, to actually
    visualize a simulation.

    :type ca:CellAutomata: See model/cell_automata.py
    :param ca:CellAutomata: See model/cell_automata.py

    :type steps:int: 
    :param steps:int: The number of simulation steps.

    :rtpye: A Simulation Result as stated in the report, see model/state.py 
"""
def pipeline(*, ca: CellAutomata , steps: int) -> SimulationResult:
    steps = steps
    timeshift = 1
    res = []
    x = ca.state.persons
    res.append(x.copy())
    evo = Evolution(ca.row_size, ca.col_size, ca.state.obstacles, ca.state.target)

    for _ in range(0, steps, timeshift):
        xi = evo.evolute(x)
        res.extend(xi.copy())
        x = xi[-1].copy()
    return SimulationResult(step = res)
