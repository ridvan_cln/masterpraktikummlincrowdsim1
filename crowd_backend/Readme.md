# CrowdSimCA (crowd_sim_ca)

Crowd Simulation Backend

## Install the dependencies
```bash
poetry install
```

### Start the backend in development mode (hot-code reloading, error reporting, etc.)
```bash
poetry run uvicorn main:app --host 0.0.0.0 --port 8000 --reload
```