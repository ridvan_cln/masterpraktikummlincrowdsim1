import uvicorn

from fastapi import FastAPI
from app.api.api import api_router
from fastapi.middleware.cors import CORSMiddleware


# import typing as T
# from pydantic import BaseModel


app = FastAPI()

origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(api_router)

#debugging
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)