# MasterPraktikumMLinCrowdSim1

To this Project to work, please install node.js 12:

https://quasar.dev/quasar-cli/installation
```bash
npm install -g @quasar/cli
```

Also python 3.7 is needed and Poetry:

https://python-poetry.org/docs/
```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

After that, follow the instructions in crowd_backend or frontend respectively.